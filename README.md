# alta3research-ansible-cert

Ansible project for Certification from Alta3 Research

Role Name
=========

Get the temperature of a City from https://api.openweathermap.org and output a templated file in user home directory.

Requirements
------------

- The api key could be generated from https://api.openweathermap.org from sign in user, for testing purpose the api key is encrypted in this project. 
- However, this API key will be deactivated after Author has obtained certification :)

Role Variables
--------------
- city : city variables could be passed in as extra variables when run the playbook. Else, you will be prompt to provide the city.
- api_key : the api key could be generate from https://api.openweathermap.org after sign in as user.
- Encryption key for the vault is stored in files directory

Dependencies
------------
- https://api.openweathermap.org up and running
- API Key is required to call the API. 

Example Playbook
----------------

ansible-playbook alta3research-ansiblecert01.yml -e "city=London" --vault-password-file=WeatherChecker/files/.vaultpw


License
-------

BSD

Author Information
------------------

Au Tin Choo
